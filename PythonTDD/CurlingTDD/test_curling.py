import unittest

from curling import OneTeam
from curling import TwoTeams


class GameTest(unittest.TestCase):
    def setUp(self):
        self.game = OneTeam()

    def test_distance_from_center(self):
        self.game.score(95)
        self.assertEqual(5, self.game.points())
    
    def test_distance_over_center(self):
        self.game.score(105)
        self.assertEqual(5, self.game.points())
    
    def test_game(self):
        self.game.score(95)
        self.game.score(93)
        self.game.score(92)
        self.game.score(92)
        self.game.score(103)
        self.game.score(99)
        self.assertEqual(1, self.game.roll())


class GameTest2(unittest.TestCase):
    def setUp(self):
        self.game = TwoTeams()

    def test_blue_wins(self):
        self.game.red(95)
        self.game.blue(96)
        self.game.red(92)
        self.game.blue(93)
        self.game.red(97)
        self.game.blue(99)
        self.assertEqual("Blue", self.game.winner())

    def test_red_wins(self):
        self.game.red(95)
        self.game.blue(96)
        self.game.red(92)
        self.game.blue(93)
        self.game.red(99)
        self.game.blue(97)
        self.assertEqual("Red", self.game.winner())
    
    def test_tie(self):
        self.game.red(95)
        self.game.blue(96)
        self.game.red(92)
        self.game.blue(93)
        self.game.red(99)
        self.game.blue(99)
        self.assertEqual("Tie", self.game.winner())