class Game(object):

    def __init__(self):
        self.rolls = []

    def roll(self, pins):
        self.rolls.append(pins)

    def total_score(self):
        score = 0
        index = 0

        for frame in range(10):
            if self.is_strike(index):
                score += 10 + self.rolls[index+1] + self.rolls[index+2]
                index += 1
            elif self.is_spare(index):
                score += 10 + self.rolls[index+2]
                index += 2
            else:
                score += self.rolls[index] + self.rolls[index+1]
                index += 2
        return score

    def is_spare(self, index):
        return self.rolls[index] + self.rolls[index+1] == 10

    def is_strike(self, index):
        return self.rolls[index] == 10