import re

class Polynomial:
    def __init__(self, poly_str):
        self.poly_str = poly_str
        self.getTerms()

    def __str__(self):
        result = ''
        first_term = 0
        for exponent in sorted(self.results.keys(), reverse=True):
            coefficient = self.results[exponent]
            if coefficient > 0 and first_term != 0:
                result = result + " + "
            elif first_term != 0:
                result = result + " "
            first_term = 1
            result = result + str(coefficient)
            if exponent != 0:
                result = result + "x"
                if exponent != 1:
                    result = result + "^" + str(exponent)
        result = result.replace('-', '- ')
        result = result.replace('1x', 'x')
        return result


    def getTerms(self):
        #Remove Spaces to make regular expression easier
        self.poly_str = (self.poly_str.replace(' ', ''))

        # Separates each term into 5 parts  
        # 0 = + or - | 1 = coefficient  | 2 = x^exponent | 3 = ^exponent | 4 = exponent
        regex = re.compile(r"([+-]*)(\d+)?(x(\^(\d+))?)?")
        # Separate a string into individual terms 
        terms = regex.findall(self.poly_str)

        # Removes Empty Match at End
        terms.pop()
        print(terms)

        # Create a dictionary to save results
        self.results = {}
        
        # Logic for Exponent and Coefficient
        for term in terms:
            # if not coefficient then coefficent = 1
            if term[1] is '':
                coefficient = 1
            # the coefficient is the coefficient that it seperated in the regex.findall above
            else:
                coefficient = int(term[1])
            # Checking if - sign is present and makes the coefficient negative
            if term[0] == '-':
                coefficient = -coefficient
            # If null then the power is 0 used with a number without x
            if term[2] is '':
                exponent = 0
            # If null then power is one used with variable x by itself
            elif term[4] is '':
                exponent = 1
            # The exponent is already specified 
            else:
                exponent = int(term[4])
            self.results[exponent] = coefficient
        print(self.results)
        
    def printPoly(self):
        print(str(self))
    
    def addPolynomial(self, poly2):
        # Loop for Every Exponent in either Dict
        for exponent in self.results.keys() | poly2.results.keys():
            # If poly2 contains exponent not present in poly1 add to poly1
            if not exponent in self.results:
                self.results[exponent] = poly2.results[exponent]
            # If exponent in both add poly1 and poly2
            elif exponent in poly2.results:
                self.results[exponent] = self.results[exponent] + poly2.results[exponent]
                # Checks if addition of two numbers is zero 
                if self.results[exponent] == 0:
                    del self.results[exponent]
    
    def subPolynomial(self, poly2):
        # Loop for Every Exponent in either Dict
        for exponent in self.results.keys() | poly2.results.keys():
            # If poly2 contains exponent not present in poly1 add to poly1
            if not exponent in self.results:
                self.results[exponent] = poly2.results[exponent]
            # If exponent in both add poly1 and poly2
            elif exponent in poly2.results:
                self.results[exponent] = self.results[exponent] - poly2.results[exponent]
                # Checks if addition of two numbers is zero 
                if self.results[exponent] == 0:
                    del self.results[exponent]


p1 = Polynomial("5x^4 + 2x + 10")
p1.printPoly()
print("\n")

p2 = Polynomial("3x^4 + 2x + 5")
p2.printPoly()
print("\n")

p1.subPolynomial(p2)
p1.printPoly()        


       
        