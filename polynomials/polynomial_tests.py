from unittest import TestCase

from polynomial import Polynomial

class PolynomialTestSuite(TestCase):
    def test_create_polynomial(self):
        self.p1 = Polynomial('2x^2 + 2x + 2')
        self.assertEqual(str(self.p1), '2x^2 + 2x + 2')
    
    def test_create_polynomial_without_space_between_terms(self):
        self.p2 = Polynomial('7x^3+4x^2+x')
        self.assertEqual(str(self.p2), '7x^3 + 4x^2 + x')
    
    def test_add_polynomial(self):
        self.p1 = Polynomial('2x^2 + 2x + 2')
        self.p2 = Polynomial('7x^3+4x^2+x')
        self.p1.addPolynomial(self.p2)
        self.assertEqual(str(self.p1), '7x^3 + 6x^2 + 3x + 2')