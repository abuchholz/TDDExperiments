import unittest

from archery import Game


class GameTest(unittest.TestCase):
    def setUp(self):
        self.game = Game()

    def test_bulleyes(self):
        self.game.shoot(10)
        self.assertEqual(10, self.game.point())

    def test_miss(self):
        self.game.shoot(0)
        self.assertEqual(0, self.game.point())

    def test_play_game(self):
        self.game.shoot(1)
        self.game.shoot(10)
        self.game.shoot(9)
        self.game.shoot(2)
        self.assertEqual(22, self.game.point())


