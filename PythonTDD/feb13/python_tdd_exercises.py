import os 


def reverse_list(list):
    reversedlist = []
    for item in list:
        reversedlist = [item] + reversedlist
    return reversedlist

def reverse_string(string):
    reversedstring = ''
    for item in string:
        reversedstring = item + reversedstring
    return reversedstring

def is_english_vowel(vowel):
    if vowel in 'aeiouyAEIOUY':
        return True
    else:
        return False

def count_num_vowels(sentence):
    count = 0
    for vowel in sentence:
        if vowel in 'aeiouyAEIOUY':
            count += 1
    return count

def histogram(l):
    histo = ''
    for i, d in enumerate(l):
        histo += ('#' * d)
        if not (i + 1 == len(l)):
            histo += '\n'
    return histo

def get_word_lengths(s):
    counter = 0
    lengths = []
    for i in s:
        if i == ' ':
            lengths += [counter]
            counter = 0
        else:
            counter += 1
    lengths += [counter]
    return lengths

def find_longest_word(text):
    counter = 0
    lengths = []
    word = ''
    words = []
    for i in text:
        if i == ' ':
            if counter > max(lengths + [0]):
                longest = word
            lengths += [counter]
            counter = 0
            words += [word]
            word = ''
        else:
            counter += 1
            word += i
    lengths += [counter]
    words += [word]
    return longest

def validate_dna(dna):
    for i in dna:
        if not i in 'actgACTG':
            return False
    return True

def base_pair(dna):
    dnamap = {'a':'t', 't':'a', 'g':'c', 'c':'g', 'A':'t', 'T':'a', 'G':'c', 'C':'g'}
    if dna in dnamap:
        return dnamap[dna]
    else:
        return 'unknown'

def transcribe_dna_to_rna(dna):
    validate_dna(dna)
    rnamap = {'a':'A', 't':'U', 'g':'G', 'c':'C', 'A':'A', 'T':'U', 'G':'G', 'C':'C'}
    rna = ''
    for i in dna:
        rna += rnamap[i]
    return rna

def get_complement(dna):
    cmap = {'a':'T', 't':'A', 'g':'C', 'c':'G', 'A':'T', 'T':'A', 'G':'C', 'C':'G'}
    c = ''
    for i in dna:
        c += cmap[i]
    return c

def get_reverse_complement(dna):
    cmap = {'a':'T', 't':'A', 'g':'C', 'c':'G', 'A':'T', 'T':'A', 'G':'C', 'C':'G'}
    c = ''
    for i in dna:
        c = cmap[i] + c
    return c

def remove_substring(ss, s):
    new = ''
    i = 0
    while i < len(s):
        d = 0
        matches = True
        while d < len(ss):
            if i + d >= len(s):
                matches = False
                d = 10000
            else:
                if not s[i + d] == ss[d]:
                    matches = False
            d += 1
        if matches:
            i += d
        else:
            new += s[i]
            i += 1
    return new

def get_position_indices(triplet, dna):
    new = ''
    positions = []
    i = 0
    while i < len(dna):
        d = 0
        matches = True
        while d < len(triplet):
            if i + d >= len(dna):
                matches = False
                d = 10000
            else:
                if not dna[i + d] == triplet[d]:
                    matches = False
            d += 1
        if matches:
            positions += [(i/3)]
            i += d
        else:
            new += dna[i]
            i += 1
    return positions

def get_3mer_usage_chart(s):
    result = []
    threemers = ['AAG', 'ACT', 'AGA', 'AGC', 'AGG', 'CCG', 'CGG', 'CTT', 'GAA', 'GAG', 'GCT', 'GGA', 'TAC', 'TAG', 'TTA']
    for threemer in threemers:
        amount = len(get_position_indices(threemer, s))
        result += [(threemer, amount)]
    return result

def read_column(file_name, column):
    f = open(file_name, "r")
    returnlist = []
    for i in f.readlines():
        s = 0
        l = 0
        while s < column - 1:
            while i[l] != " ":
                l += 1
            while i[l] == " ":
                l += 1
            s += 1
        k = l
        while i[k] != " ":
            k += 1
        returnlist += [""]
        returnlist[-1] += i[l:k]
        returnlist[-1] = float(returnlist[-1])
    f.close()
    return returnlist
